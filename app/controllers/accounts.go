package controllers

import (
	"fmt"
	"github.com/revel/revel"
	"restaurant-order-app/app/models"
)

type Accounts struct {
	*revel.Controller
}

func (c Accounts) Create() revel.Result {
	return c.Render()
}

func (c Accounts) CreatePost() revel.Result {
	var account models.Account
	c.Params.Bind(&account, "account")
	c.Validation.Required(account.FirstName)
	c.Validation.Required(account.LastName)
	c.Validation.Required(account.Address1)
	c.Validation.Required(account.Address2)
	c.Validation.Required(account.City)
	c.Validation.Required(account.State)
	c.Validation.Required(account.ZipCode)
	c.Validation.Length(account.ZipCode, 5)
	if c.Validation.HasErrors() {
		c.FlashParams()
		fmt.Printf("Has Error: true\n")
		fmt.Printf("Account info: %v\n", account)
		return c.RenderTemplate("accounts/create.html")
	} else {
		c.Flash.Success("Account created.!!!")
		return c.Redirect(App.Index)
	}
}
